#!/usr/bin/env bash

<<CALCULATOR

The program is used to calculate the sum,
substract, product or quotient of two integers

CALCULATOR

echo -n "Give a first integer: "
read int1
echo -n "Give a second integer: "
read int2
echo "Select operation:"
echo -n "1-Addition 2-Substraction 3-Multiplication 4-Division: "
read int3

case ${int3} in
	"1")
		((sum=${int1} + ${int2}))
		echo "${int1} + ${int2} = ${sum}"
		;;
	"2")
		((sub=${int1} - ${int2}))
		echo "${int1} - ${int2} = ${sub}"
		;;
	"3")
		((product=${int1} * ${int2}))
		echo "${int1} * ${int2} = ${product}"
		;;
	"4")
		((quotient=${int1} / ${int2}))
		((modulo=${int1}%${int2}))
		echo "${int1} / ${int2} = ${quotient}; modulo = $modulo"
		;;
	*)
		echo "Wrong selection!"
		;;
esac
